<!doctype html>
<html lang="en">
  <head>
    <title>Explore</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 
    <style>
     body{
            background: rgb(20,90,114);
            background: linear-gradient(90deg, rgba(20,90,114,0.5219129888283438) 3%, rgba(20,90,116,1) 52%, rgba(20,90,114,0.4462827367275035) 100%);
        }
        input { 
    border-radius: 20px;
    padding: 5px;
    border: 2px solid #28A745;
    width:80px;
  }

input:nth-of-type(1):focus {
    outline: 0;
    box-shadow: 0 0 10px #28A745;
    text-align: center;
}
    .btn{
        border-color:#fff;
        width:80px;
    }
    img{
        margin-left:60px;
    }
    </style>
  </head>
  <body >
      

    <div id="play" class="container"></div>
    <div class="container" style="margin-top:10%;">
        <div class="row">
            <div class="col col-sm"></div>
            <div class="col col-sm">
                <img src="images/exploren.gif" class="img-fluid rounded-circle" alt="" width="170" height="170">
            </div>
            <div class="col col-sm"></div>
        </div>

        <div class="row mt-4">
            <div class="col-12 col-sm"></div>
            <div class="col-12 col-sm-5" >
                <div class="d-flex flex-sm-row justify-content-around">
                <a  class="btn btn-outline-success text-white exploreItem" role="button" href="music.php" data-value="music">Music</a>
                <a class="btn btn-outline-success text-white exploreItem ml-sm-1 mr-sm-1"  role="button" href="news.php"data-value="news">News</a>
                <a class="btn btn-outline-success text-white exploreItem mr-sm-1" role="button" href="book.php" data-value="book">Book</a>
                <a  class="btn btn-outline-success text-white exploreItem" role="button" href="story.php" data-value="stories">Stories</a>
                </div>
            </div>
            <div class="col-12 col-sm"></div>
        </div>        
    </div>
    </div> 
        <center><input type="text" class="mt-3" id="language_number" autofocus ></center>       
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/getSpeech.js"></script>
    <script>
      $(document).ready(function(){
        var select_lang = localStorage.getItem("selected_lang");
        var eng = ["press one for music", "press two for news", "press three for book", "press four for stories"];
        var hin = ["गाना सुनने के लिए एक दबाएं ", "समाचार जानने के लिए दो दबाएं ", "किताबों के लिए तीन दबाएं", "कहानियों के लिए चार दबाएं"];

        if(select_lang == 'hi'){
          MenuData(hin, select_lang);
        }
        else{
          MenuData(eng, select_lang);
        }
        
        var pages = ['music.php', 'news.php', 'book.php', 'story.php'];
        $('#language_number').keyup(function(){
            var p = $('#language_number').val();
            if(p >= 1 && p <= pages.length){
              goto_page(pages, p-1);
;            }
        });
        
      });
    </script>
  </body>
</html>