<!doctype html>
<html lang="en">
  <head>
    <title>Home</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <style>
      *{
        margin:0px;
        padding: 0px;
      }
      #loading{
        position: fixed;
        width:100%;
        height:100vh;
        background: #fff url('loader.gif') no-repeat center;
        z-index:9999;

      }
      body{
        background: rgb(81,162,234);
        background: linear-gradient(90deg, rgba(81,162,234,1) 0%, rgba(226,183,137,1) 51%, rgba(81,162,234,1) 100%);
      }

      input { 
    border-radius: 20px;
    padding: 5px;
    border: 2px solid #28A745;
    width:80px;
  }

input:nth-of-type(1):focus {
    outline: 0;
    box-shadow: 0 0 10px #28A745;
    text-align: center;
}
.btn{
  width:80px;
  border-color:white;
}
    </style>
  </head>
  <body>
      
    <div id="loading"></div>
    <div id="play" class="container"></div>
    <div class="container" style="margin-top:10%;">
        <div class="row">
            <div class="col-3 col-sm-5"></div>
            <div class="col-7 col-sm">
                <img src="girl1.png" class="img-fluid" alt="" width="170" height="170">
            </div>
            <div class="col-2 col-sm"></div>
        </div>

        <div class="row mt-4">
            <div class="col-12 col-sm"></div>
            <div class="col-12 col-sm-5" >
                <div class="d-flex flex-sm-row justify-content-around" id="lanMenu">
                  <a  class="btn text-white btn-outline-success lang" role="button" href="menu.php" data-value="hi">हिन्दी</a>
                  <a  class="btn text-white btn-outline-success lang" role="button" href="menu.php" data-value="en">English</a>
                </div>
            </div>
            <div class="col-12 col-sm"></div>
            
        </div> 
        <center><input type="text" class="mt-3" id="language_number"  autofocus></center>       
    </div>

    <audio  id="loadMusic">
      <source src="song.mpeg"  type="audio/ogg" >
     Your browser does not support the audio element.
     </audio>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/getSpeech.js"></script>
    <script src="js/events.js"></script>

    <script>
          // calling first time function to select language defined in getSpeech.js
          $(document).ready(function(){
              // this is to hide website preload from home page 
              calling();
              function calling(){
                setTimeout(function(){ Myfunction(); }, 1000);
              }

              function Myfunction(){
                  $('#loading').css('display', 'none');
              }

                // calling first time function to select language defined in getSpeech.js
                language();

                $('.lang').click(function(event){
                    event.preventDefault();
                    selected_lang = $(this).attr('data-value');
                    localStorage.setItem("selected_lang", selected_lang);
                    window.location.href = "menu.php";
                    return false;
              });
              
              $('#language_number').keyup(function(){
                  if($('#language_number').val() == 1){
                    localStorage.setItem("selected_lang", 'hi');
                    window.location.href = "menu.php";
                    return false;
                  }
                  else if($('#language_number').val() == 2){
                    localStorage.setItem("selected_lang", 'en');
                    window.location.href = "menu.php";
                    return false;
                  }
              });

          });

    </script>
  </body>
</html>