<!doctype html>
<html lang="en">
  <head>
    <title>Menu</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        body{
            background: rgb(20,62,114);
            background: linear-gradient(90deg, rgba(20,62,114,1) 3%, rgba(69,114,154,1) 52%, rgba(20,90,114,1) 100%);
        }
        input { 
    border-radius: 20px;
    padding: 5px;
    border: 2px solid #28A745;
    width:80px;
  }

input:nth-of-type(1):focus {
    outline: 0;
    box-shadow: 0 0 10px #28A745;
    text-align: center;
}
    .btn{
        border-color:#fff;
        width:100px;
    }
    img{
        margin-left:60px;
    }
    </style>
  </head>
  <body>

  <div id="play" class="container"></div>
    <div class="container" style="margin-top:10%;">
        <div class="row">
            <div class="col-3 col-sm"></div>
            <div class="col-7 col-sm">
                <img src="images/menu.gif" class="img-fluid rounded-circle" alt="" width="170" height="170">
            </div>
            <div class="col-2 col-sm"></div>
        </div>

        <div class="row mt-4">
            <div class="col-12 col-sm"></div>
            <div class="col-12 col-sm" >
                <div class="d-flex flex-sm-row justify-content-around" id="lanMenu">
                  <a class="btn btn-outline-success text-white menu" href="emergency.php" role="button" >Emergency</a>
                  <a class="btn btn-outline-success text-white menu ml-1 mr-1" href="explore.php" role="button">Explore</a>
                  <a  class="btn btn-outline-success text-white menu mr-1" href="help.php" role="button" >Help</a>
                  <a  class="btn btn-outline-success text-white menu" href="about.php" role="button">About us</a>
                </div>
            </div>
            <div class="col-12 col-sm"></div>
        </div>        
    </div>
    </div> 
        <center><input type="text" class="mt-3" id="language_number" autofocus ></center>       
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/getSpeech.js"></script>
    <script>
      $(document).ready(function(){
        var select_lang = localStorage.getItem("selected_lang");
        var eng = ["press one for emergency", "press two for explore", "press three for help", "press four for about"];
        var hin = ["आपातकालीन  के लिए एक दबाएं", "एक्स्प्लोर करने के लिए दो दबाएं ", "मदद के लिए तीन दबाएं ", "अबाउट के लिए चार दबाएं"];

        if(select_lang == 'hi'){
          MenuData(hin, select_lang);
        }
        else{
          MenuData(eng, select_lang);
        }
        
        
        var pages = ['emergency.php', 'explore.php', 'help.php', 'about.php'];
        $('#language_number').keyup(function(){
            var p = $('#language_number').val();
            if(p >= 1 && p <= pages.length){
              goto_page(pages, p-1);
          }
        });
        
      });
    </script>
  </body>
</html>