<!doctype html>
<html lang="en">
  <head>
    <title>Music</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<style>
    body{

        background: rgb(201,64,163);
        background: linear-gradient(90deg, rgba(201,64,163,0.9448821765034139) 3%, rgba(31,57,84,0.9364788151588761) 49%, rgba(13,93,110,1) 100%);
    }
    img{
        margin-left:63px;
    }
    input { 
    border-radius: 20px;
    padding: 5px;
    border: 2px solid #28A745;
    width:80px;
  }

input:nth-of-type(1):focus {
    outline: 0;
    box-shadow: 0 0 10px #28A745;
    text-align: center;
}
.btn{
    width:80px;
}
</style>
</head>
  <body>
      

    <div class="container" style="margin-top:10%;">
        <div class="row">
            <div class="col-3 col-sm">
            <audio controls id="local" preload="none">
                <source src="song.mpeg" type="audio/mpeg">
            </audio>
            <button id="local_suffle" type="button" class="btn btn-success">change</button>
            </div>
            <div class="col-7 col-sm">
                <img src="images/music.gif" class="img-fluid rounded-circle" alt="" width="170" height="170">
            </div>
            <div class="col-2 col-sm">
            <audio controls preload="none" id="online">
                <source src="http://free-mobi.org/files/ringtones/1/by_richy.mp3" type="audio/mpeg">
            </audio>
            <button id="online_suffle" type="button" class="btn btn-success">change</button>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-12 col-sm"></div>
            <div class="col-12 col-sm-5" >
                <div class="d-flex flex-sm-row justify-content-around">
                  <button type="button" class="btn btn-success mus" data-value="local">Local </button>
                  <button type="button" class="btn btn-success mus" data-value="online">Online</button>
                </div>
            </div>
            <div class="col-12 col-sm"></div>
            
        </div> 
        <center><input type="text" class="mt-3" id="language_number"  autofocus></center>       
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/getSpeech.js"></script>

    <script>

        var select_lang = localStorage.getItem("selected_lang");
        var eng = ["press one for local music", "press two for online"];
        var hin = ["लोकल गाना  के लिए एक दबाएं", "ऑनलाइन गाना  के लिए दो दबाएं "];

        if(select_lang == 'hi'){
          MenuData(hin, select_lang);
        }
        else{
          MenuData(eng, select_lang);
        }

        $('.mus').click(function(){
          var loc_onl = $(this).attr('data-value');
            if(loc_onl == 'local'){
              $("#local").trigger('load');
              $("#local").trigger('play');
            }
            else if(loc_onl == 'online'){
              $("#online").trigger('load');
              $("#online").trigger('play'); 
            }
            
        });

        $('#language_number').keyup(function(){
            if($('#language_number').val() == 1){
              $("#local").trigger('load');
              $("#local").trigger('play');
            }
            else if($('#language_number').val() == 2){
              $("#online").trigger('load');
              $("#online").trigger('play');
            }
        });

        var src = [
          "http://free-mobi.org/files/ringtones/1/by_richy.mp3",
          "http://free-mobi.org/files/4/sleepy.mp3",
          "http://free-mobi.org/files/4/good_morning_alarm.mp3",
          "http://free-mobi.org/files/ringtones/1/otis_redding.mp3"
        ];

        $('#online_suffle').on('click', function (e) {
            var rand = Math.floor(Math.random() * src.length);
            $('#online source').attr("src", src[rand])  
            $('#online')[0].load();
            $('#online')[0].play();
      });

      var local_src = [
          "song.mpeg",
          "song.mpeg"
        ];
      $('#local_suffle').on('click', function (e) {
            var rand = Math.floor(Math.random() * local_src.length);
            $('#local source').attr("src", local_src[rand])  
            $('#local')[0].load();
            $('#local')[0].play();
      });


    </script>
  </body>
</html>