<!doctype html>
<html lang="en">
  <head>
    <title>News</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  
    <style>
        body{
            background: rgb(201,89,64);
            background: linear-gradient(90deg, rgba(201,89,64,0.9448821765034139) 3%, rgba(31,68,84,0.9364788151588761) 62%, rgba(110,13,53,1) 100%);
        }
        .btn{
            width:80px;
        }
        img{
        margin-left:55px;
    }
    input { 
    border-radius: 20px;
    padding: 5px;
    border: 2px solid #28A745;
    width:80px;
  }

input:nth-of-type(1):focus {
    outline: 0;
    box-shadow: 0 0 10px #28A745;
    text-align: center;
}
    </style>
</head>
  <body>
  <div id="play" class="container"></div>
    <div class="container" style="margin-top:10%;">
        <div class="row">
            <div class="col-3 col-sm">
            <audio controls id="local" preload="none">
                <source src="song.mpeg" type="audio/mpeg">
            </audio>
            </div>
            <div class="col-7 col-sm">
                <img src="images/news.gif" class="img-fluid rounded-circle" alt="" width="170" height="170">
            </div>
            <div class="col-2 col-sm"></div>
        </div>

        <div class="row mt-4">
            <div class="col-12 col-sm"></div>
            <div class="col-12 col-sm-5" >
                <div class="d-flex flex-sm-row justify-content-around" id="lanMenu">
                  <button type="button" class="btn btn-success lang" data-value="hi">हिन्दी</button>
                  <button type="button" class="btn btn-success lang" data-value="en">English</button>
                </div>
            </div>
            <div class="col-12 col-sm"></div>
            
        </div> 
        <center><input type="text" class="mt-3" id="language_number"  autofocus></center>       
    </div>
      
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>